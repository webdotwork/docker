CREATE DATABASE test_db;
-- USE DATABASE test_db;
CREATE USER "test-admin-user" WITH PASSWORD 'q';
CREATE USER "test-simple-user" WITH PASSWORD 'q';
CREATE TABLE orders (id SERIAL PRIMARY KEY, наименование VARCHAR(20), цена INTEGER);
-- DROP TABLE orders;
CREATE TABLE clients (id SERIAL PRIMARY KEY, фамилия VARCHAR(20), "страна проживания" VARCHAR(30), заказ INTEGER, CONSTRAINT fk_заказ FOREIGN KEY(заказ) REFERENCES orders(id));
-- DROP TABLE clients;
-- GRANT ALL on test_db to test_admin_user;
-- SHOW GRANT FOR test_admin_user;
-- grant all privileges on database test_db to test_admin_user;
GRANT ALL ON TABLE orders, clients TO "test-admin-user";
-- GRANT CONNECT ON DATABASE test_db TO "test_admin_user";
CREATE INDEX ON clients("страна проживания");
GRANT CONNECT ON DATABASE test_db TO "test-simple-user";
GRANT SELECT, INSERT, UPDATE, DELETE ON orders, clients TO "test-simple-user";
-- SELECT * FROM information_schema.role_table_grants WHERE table_catalog='test_db' AND table_schema='public' ORDER BY grantee ASC;
INSERT INTO orders VALUES (1, 'Шоколад', 10), (2, 'Принтер', 3000), (3, 'Книга', 500), (4, 'Монитор', 7000), (5, 'Гитара', 4000);
-- SELECT count(1) FROM orders;
INSERT INTO clients VALUES (1, 'Иванов Иван Иванович', 'USA'), (2, 'Петров Петр Петрович', 'Canada'), (3, 'Иоганн Себастьян Бах', 'Japan'), (4, 'Ронни Джеймс Дио', 'Russia'), (5, 'Ritchie Blackmore', 'Russia');
-- SELECT * FROM clients;
UPDATE clients SET "заказ" = (SELECT ID FROM orders WHERE "наименование"='Книга') WHERE "фамилия"='Иванов Иван Иванович';
UPDATE clients SET "заказ" = (SELECT ID FROM orders WHERE "наименование"='Монитор') WHERE "фамилия"='Петров Петр Петрович';
UPDATE clients SET "заказ" = (SELECT ID FROM orders WHERE "наименование"='Гитара') WHERE "фамилия"='Иоганн Себастьян Бах';
-- SELECT "фамилия" FROM clients INNER JOIN orders ON clients."заказ" = orders.ID;
-- SELECT c.* FROM clients c JOIN orders o ON c.заказ = o.id;

-- EXPLAIN SELECT "фамилия" FROM clients INNER JOIN orders ON clients."заказ" = orders.ID;
-- (1. Построчно прочитана таблица orders
-- 2. Создан кеш по полю id для таблицы orders
-- 3. Прочитана таблица clients
-- 4. Для каждой строки по полю "заказ" будет проверено, соответствует ли она чему-то в кеше orders
-- - если соответствия нет - строка будет пропущена
-- - если соответствие есть, то на основе этой строки и всех подходящих строках кеша СУБД сформирует вывод
-- При запуске просто explain, Postgres напишет только примерный план выполнения запроса и для каждой операции предположит:
-- - сколько процессорного времени уйдёт на поиск первой записи и сбор всей выборки: cost=первая_запись..вся_выборка
-- - сколько примерно будет строк: rows
-- - какой будет средняя длина строки в байтах: width
-- Postgres делает предположения на основе статистики, которую собирает периодический выполня analyze запросы на выборку данных из служебных таблиц.
-- Если запустить explain analyze, то запрос будет выполнен и к плану добавятся уже точные данные по времени и объёму данных.
-- explain verbose и explain analyze verbose - для каждой операции выборки будут написаны поля таблиц, которые в выборку попали.)

-- pg_dump -U postgres -F t test_db > /var/lib/postgresql/backup/test_db.tar
-- # createdb -U postgres test_db
-- # pg_restore -U postgres --dbname=test_db --verbose /var/lib/postgresql/backup/test_db.tar